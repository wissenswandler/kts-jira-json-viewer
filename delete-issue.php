<?php

include 'domain.php';

$domain = $_GET["domain"];
$issue  = $_GET["issue" ];
$rdf_resource = '<https://' . $domain . '/rest/api/latest/issue/' . $issue . '>';

$sparql_update_operation = <<<EOD
DELETE
{
  ?s ?p ?o
}
WHERE
{
 {
  bind( $rdf_resource as ?s )
  ?s ?p ?o
 }
 UNION
 {
  bind( $rdf_resource as ?o )
  ?s ?p ?o
  MINUS{ ?p a <http://knowledge-transformation.org/2018/kts#SLOP> }
 }
}
EOD;

echo "submitting SPARQL update...";
echo $sparql_update_operation;

$ch = curl_init();

curl_setopt($ch, CURLOPT_URL,            $sparql_update_endpoint );
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 );
curl_setopt($ch, CURLOPT_POST,           1 );
curl_setopt($ch, CURLOPT_POSTFIELDS,     'update=' . urlencode( $sparql_update_operation )  ); 
curl_setopt($ch, CURLOPT_HTTPHEADER,     array('Content-Type: application/x-www-form-urlencoded') ); 

$result=curl_exec ($ch);

echo "... done with result: " . $result;

if (curl_errno($ch)) 
{
	echo 'Error:' . curl_error($ch);
}
curl_close ($ch);	

?>
