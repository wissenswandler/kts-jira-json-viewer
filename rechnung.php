<?php
/*
 * module to present the JSON-Information as invoice document
 *
 * intended audience is the invoice recipient or 'public'
 *
 * this is pretty much a constant template with little dynamic recursion or expansion,
 * because we dont' want to give away internals 
 * if the underlying object (issue) has more information than "need to have" for the invoice purpose
 */

function warning_handler($errno, $errstr) { 
	throw new Exception( $errstr );
}

/*
 * understand the requested resource
 */
$self = $_GET["domain"] . '/rest/api/2/issue/' . $_GET[ issue ];

/*
 * lookup JSON for requested resource
 */
try {
	set_error_handler("warning_handler", E_WARNING);
	$subject = json_decode( file_get_contents( '/volume1/webhook/' . $self ), true );
	restore_error_handler();
}
catch( Exception $e)
{
	http_response_code(404);
	include( 'include/error.php' );	
	die();	
}

/*
 * render requested resource to (generic) XHTML
 */
ob_start();
include 'include/html-template-invoice.php';
$xhtmlstring = ob_get_contents();
ob_end_clean();

if( $_GET["accept"] == "x" )
{
	/*
	 * translate resource from XHTML to RDF
	 */
	$xslDoc = new DOMDocument();
	$xslDoc->load( "include/invoice.xsl");

	$xmlDoc = new DOMDocument();
	$xmlDoc->loadXML( $xhtmlstring );

	$proc = new XSLTProcessor();
	$proc->importStylesheet($xslDoc);
	
	$xsl_result = $proc->transformToXML($xmlDoc);
	
	/*
	 * return transformed HTML
	 */
	echo $xsl_result;
}
else
{
	/*
	 * return XHTML for preivew in browser
	 */
	header('Content-type: application/xhtml+xml');
	echo $xhtmlstring;
}

?>