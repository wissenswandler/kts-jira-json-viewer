#
# execute in webchunk/[..]/rest/api/issue folder
#

shopt -s extglob

jq -sc \
'map( { "s" : .fields.summary , "k" : .key } ) | group_by( .s ) | .[] | { "s" : .[0].s , "k" : map( .k ) }' \
issue/!(*-*) \
> index_summary_key.json
