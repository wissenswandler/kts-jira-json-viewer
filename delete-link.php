<?php

include 'domain.php';

echo $_SERVER['QUERY_STRING'];

$domain			= $_GET["domain"		];
$sourceIssueId		= $_GET["sourceIssueId"		];
$issueLinkTypeId	= $_GET["issueLinkTypeId"	];
$destinationIssueId	= $_GET["destinationIssueId"	];

$rdf_s = "<https://" . $domain . "/rest/api/latest/issue/"		. $sourceIssueId	. ">";
$rdf_p = "<https://" . $domain . "/rest/api/latest/issueLinkType/"	. $issueLinkTypeId	. ">";
$rdf_o = "<https://" . $domain . "/rest/api/latest/issue/"		. $destinationIssueId	. ">";

$sparql_update_operation = <<<EOD
DELETE
{
  $rdf_s $rdf_p $rdf_o
}
WHERE
{
}
EOD;

echo $sparql_update_operation;

$ch = curl_init();

curl_setopt($ch, CURLOPT_URL,            $sparql_update_endpoint );
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 );
curl_setopt($ch, CURLOPT_POST,           1 );
curl_setopt($ch, CURLOPT_POSTFIELDS,     'update=' . urlencode( $sparql_update_operation )  ); 
curl_setopt($ch, CURLOPT_HTTPHEADER,     array('Content-Type: application/x-www-form-urlencoded') ); 

$result=curl_exec ($ch);

echo $result;

if (curl_errno($ch)) 
{
	echo 'Error:' . curl_error($ch);
}
curl_close ($ch);
?>
