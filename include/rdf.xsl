<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:h="http://www.w3.org/1999/xhtml"
                xmlns:kts="http://knowledge-transformation.org/2018/kts#"
                xmlns:jira="http://knowledge-transformation.org/2018/jira#"

                xmlns:func="http://exslt.org/functions"
                exclude-result-prefixes="kts"
                extension-element-prefixes="func"
>
    <xsl:output method="text"/>

    <xsl:param name="reduce"/>

    <!--			-->
    <!-- QUALITY CHECKER	-->
    <!--			-->
    <xsl:template match="*|/">
	<xsl:text>
# ELEMENT NOT HANDLED: </xsl:text>
        <xsl:value-of select=" name( . ) "/>
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:variable name="singlesinglequote">'</xsl:variable>
    <xsl:variable name="triplesinglequotes">'''</xsl:variable>

    <!--			-->
    <!-- START		-->
    <!--			-->

    <xsl:template match="/">
        <xsl:apply-templates select="h:html"/>
    </xsl:template>
    <xsl:template match="h:html">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="h:meta"/> <!-- exists in XHTML only for presentation -->

    <xsl:template match="h:head"> <xsl:text>
@prefix xsd:  &lt;http://www.w3.org/2001/XMLSchema#>              .
@prefix rdf:  &lt;http://www.w3.org/1999/02/22-rdf-syntax-ns#>    .
@prefix rdfs: &lt;http://www.w3.org/2000/01/rdf-schema#>          .
@prefix owl:  &lt;http://www.w3.org/2002/07/owl#>                 .
@prefix jira: &lt;http://knowledge-transformation.org/2018/jira#> .
@prefix kts:  &lt;http://knowledge-transformation.org/2018/kts#>  .
@base         &lt;http://knowledge-transformation.org/2018/kts>   .
</xsl:text>
    </xsl:template>

    <xsl:template match="h:body">

        <xsl:apply-templates select="h:top"/>

        <xsl:apply-templates select="h:ilinks|h:issueLink"/> <!-- skip here, handle later -->

    </xsl:template>

    <xsl:template match="h:top">

	<xsl:text>
&lt;</xsl:text>
        <xsl:value-of select=" kts:normalize_jira_url( h:self ) "/>
        <xsl:text>></xsl:text>
        <xsl:text> a </xsl:text>
        <xsl:text>&lt;</xsl:text>
        <xsl:value-of select=" kts:normalize_jira_url( ../h:fields/h:property[@key='issuetype']/h:value/h:a/@href ) "/>
        <xsl:text>></xsl:text>

        <xsl:call-template name="add_predicate_object">
            <xsl:with-param name="predicate" select=" 'jira:key' "/>
            <xsl:with-param name="object" select=" h:key "/>
            <xsl:with-param name="object_delimiter">'</xsl:with-param>
        </xsl:call-template>

        <xsl:apply-templates select="../h:pields|../h:fields"/>

        <xsl:text>
	.</xsl:text> <!-- Resource DatatypeProperties PERIOD -->

    </xsl:template>

    <xsl:template match="h:pields">
        <xsl:apply-templates/>
    </xsl:template>  <!-- both contain 'property' elements		-->
    <xsl:template match="h:fields">
        <xsl:apply-templates/>
    </xsl:template>  <!-- difference is only for CSS/HTML presentation -->

    <xsl:template match="h:masterdata">

        <xsl:if test=" $reduce = '' ">
		<xsl:text>
#
# 'master' data 
#

# status types
</xsl:text>
            <xsl:apply-templates mode="masterdata" select="//h:property[ @key='status'    ]"/>
            <xsl:text>
			
# jira-projects
</xsl:text>
            <xsl:apply-templates mode="pmasterdata" select="//h:property"/>
            <xsl:text>
			
# issue types
</xsl:text>
            <xsl:apply-templates mode="masterdata" select="//h:property[ @key='issuetype' ]"/>
            <xsl:text>
			
# link types
		
</xsl:text>
            <xsl:apply-templates mode="masterdata" select="//h:ilink"/>
        </xsl:if>
    </xsl:template>

    <!--				-->
    <!-- Specific Fields Section	-->
    <!--				-->

    <xsl:template match="h:property[ @key = 'summary' ]">
        <xsl:call-template name="add_predicate_object">
            <xsl:with-param name="predicate" select=" 'rdfs:label' "/>
            <xsl:with-param name="object_delimiter">'''</xsl:with-param>
        </xsl:call-template>
    </xsl:template>

    <xsl:template match="h:property[ @key = 'description' ]">
        <xsl:if test=" $reduce = '' ">
            <xsl:call-template name="add_predicate_object">
                <xsl:with-param name="predicate" select=" 'rdfs:comment' "/>
                <xsl:with-param name="object_delimiter">'''</xsl:with-param>
            </xsl:call-template>
        </xsl:if>
    </xsl:template>

    <xsl:template match="h:property[ @key = 'customfield_12900' ]">
        <xsl:call-template name="add_predicate_object">
            <xsl:with-param name="predicate" select=" 'kts:shape' "/>
            <xsl:with-param name="object_delimiter">'''</xsl:with-param>
        </xsl:call-template>
    </xsl:template>


    <!-- richtext fields -->
    <xsl:template match="h:property[ @key = 'customfield_10116'	]"> <!-- "Beschreibung für Kunde"	-->

        <xsl:call-template name="add_predicate_object">
            <xsl:with-param name="object_delimiter">'''</xsl:with-param>
        </xsl:call-template>

    </xsl:template>

    <!-- dateTime fields -->
    <xsl:template match="h:property[ @key = 'updated'		]">

        <xsl:call-template name="predicate_from_key"/>
        <xsl:text>"</xsl:text>
        <xsl:apply-templates select="h:value"/>
        <xsl:text>"^^xsd:dateTime</xsl:text>

    </xsl:template>

    <xsl:template match="h:property[ @key = 'duedate'		]">
        <xsl:call-template name="predicate_from_key"/>
        <xsl:text>"</xsl:text>
        <xsl:apply-templates select="h:value"/>
        <xsl:text>"^^xsd:date</xsl:text>
    </xsl:template>

    <xsl:template match="h:property[ @key = 'customfield_10063'	]"> <!-- "Start time for meetings"	-->
        <xsl:call-template name="add_predicate_object">
            <xsl:with-param name="object_delimiter">'''</xsl:with-param>
        </xsl:call-template>
    </xsl:template>

    <xsl:template match="h:property[ @key = 'customfield_10064'	]"> <!-- "Duration of meetings"	-->
        <xsl:call-template name="add_predicate_object">
            <xsl:with-param name="object_delimiter"></xsl:with-param>
        </xsl:call-template>
    </xsl:template>


    <!--				-->
    <!-- Generic Fields Section	-->
    <!--				-->

    <!-- EXCLUDED, because...	-->
    <!--	HARDCODED ELSEWHERE	-->
    <xsl:template priority="2" match="h:property[ @key = 'issuetype' ]"/> <!-- predicate 'a' -->
    <xsl:template match="h:project"/> <!-- handled in masterdata section -->
    <xsl:template match="h:status"/> <!-- handled in masterdata section -->

    <!--	OTHER REASONS -->
    <xsl:template priority="2" match="h:property[ @key = 'subtasks' ]"/> <!-- KTSJETCORE-50 -->
    <xsl:template priority="2" match="h:property[ @key = 'attachment' ]"/> <!-- Turtle predicate with no value -->

    <!--	LITTLE USE			-->
    <xsl:template priority="2"
                  match="h:property[ @key = 'customfield_10010' ]"/> <!-- "Request Type", Turtle predicate with no value -->
    <xsl:template priority="2"
                  match="h:property[ @key = 'customfield_10134' ]"/> <!-- "Lösungszeit nach Status", Turtle predicate with no value -->
    <xsl:template priority="2"
                  match="h:property[ @key = 'customfield_10073' ]"/> <!-- "SYM Werte array", Turtle predicate with no value -->
    <xsl:template priority="2"
                  match="h:property[ @key = 'customfield_10043' ]"/> <!-- COBIT Info Crit.,  Turtle predicate with no value -->
    <xsl:template priority="2"
                  match="h:property[ @key = 'customfield_10023' ]"/> <!-- "Flagged", Turtle predicate with no value -->
    <xsl:template priority="2"
                  match="h:property[ @key = 'customfield_10142' ]"/> <!-- "Bedingungen", Turtle predicate with no value -->
    <xsl:template priority="2"
                  match="h:property[ @key = 'customfield_10125' ]"/> <!-- "Lösungszeit", Turtle predicate with no value -->
    <xsl:template priority="2"
                  match="h:property[ @key = 'customfield_10126' ]"/> <!-- "Zeit bis zur ersten Antwort", Turtle predicate with no value -->
    <xsl:template priority="2"
                  match="h:property[ @key = 'customfield_10127' ]"/> <!-- "Sprache anfordern", Turtle predicate with no value -->
    <xsl:template priority="2" match="h:property[ @key = 'statuscategorychangedate']"/>
    <xsl:template priority="2" match="h:property[ @key = 'watches'		]"/>
    <xsl:template priority="2" match="h:property[ @key = 'timetracking'	]"/>
    <xsl:template priority="2" match="h:property[ @key = 'timespent'	]"/>
    <xsl:template priority="2" match="h:property[ @key = 'aggregatetimespent']"/>
    <xsl:template priority="2" match="h:property[ @key = 'aggregatetimeestimate']"/>
    <xsl:template priority="2" match="h:property[ @key = 'lastViewed'		]"/>
    <xsl:template priority="2" match="h:property[ @key = 'resolutiondate'		]"/>
    <xsl:template priority="2"
                  match="h:property[ @key = 'customfield_10012'	]"/> <!-- TODO: omit all customFieldOption -->
    <xsl:template priority="2"
                  match="h:property[ @key = 'customfield_10011'	]"/> <!-- TODO: limit customfields to their DNS domains -->
    <xsl:template priority="2"
                  match="h:property[ @key = 'customfield_10013'	]"/> <!-- TODO: limit customfields ...	-->
    <xsl:template priority="2" match="h:property[ @key = 'customfield_10020'	]"/> <!-- "[CHART] Time in Status"	-->
    <!--	BACKLOG			-->
    <xsl:template match="h:resolution"/> <!-- TO BE handled in masterdata section -->
    <xsl:template match="h:assignee"/> <!-- ... including the following properties -->
    <xsl:template match="h:creator"/> <!-- ... including the following properties -->
    <xsl:template match="h:reporter"/> <!-- ... including the following properties -->
    <xsl:template match="h:property[ @key = 'accountId'	]"/> <!-- TO BE handled in masterdata section -->
    <xsl:template match="h:property[ @key = 'accountType'	]"/> <!-- TO BE handled in masterdata section -->
    <xsl:template match="h:property[ @key = 'displayName'	]"/> <!-- TO BE handled in masterdata section -->
    <xsl:template priority="2" match="h:property[ @key = 'security'		]"/>
    <xsl:template priority="2" match="h:property[ @key = 'issuerestriction'	]"/>
    <xsl:template match="h:property[ @key = 'customfield_10113'	]"/> <!-- "Kürzel"			-->
    <xsl:template match="h:property[ @key = 'created'		]"/>
    <xsl:template match="h:property[ @key = 'labels'		]"/>
    <!--	UNKNOWN			-->
    <xsl:template priority="2"
                  match="h:property[ @key = 'customfield_10019'	]"/> <!-- TODO: limit customfields ...	-->
    <xsl:template priority="2"
                  match="h:property[ @key = 'customfield_10044'	]"/> <!-- TODO: limit customfields ... 	-->
    <xsl:template priority="2"
                  match="h:property[ @key = 'customfield_10061'	]"/> <!-- TODO: limit customfields ...	-->
    <!--	SELECTED		-->

    <!--	OPERATIONAL		-->

    <!-- 'referencing' (ObjectProperty) FIELD  -->
    <xsl:template match="h:property[ h:value//h:a[ @href ]  and  ( count( ./ancestor::* ) =3 or ancestor::h:li ) ]">
        <!-- IF this is part of an array (li element)		-->
        <!--  then don't render the numerical key indices	-->
        <!--  but only the value -->
        <!--  since iteration logic is implemented in ul / li / span templates -->
        <xsl:if test="not( ancestor::h:li )">
            <xsl:call-template name="predicate_from_key"/>
        </xsl:if>
        <xsl:apply-templates select="h:value"/>
        <xsl:text>
	# included because of 'a href' tag reference on level </xsl:text><xsl:value-of select="count( ./ancestor::* )"/>
    </xsl:template>

    <xsl:template name="predicate_from_key">
	<xsl:text>
	;</xsl:text>
        <xsl:text>  jira:</xsl:text>
        <xsl:value-of select="@key"/>
        <xsl:text>	</xsl:text>
    </xsl:template>

    <xsl:template match="h:value">
        <xsl:apply-templates/>
    </xsl:template>

    <!--			-->
    <!--  iterating arrays  -->
    <!--			-->
    <xsl:template match="h:ul">
        <xsl:apply-templates select="h:li"/>
    </xsl:template>
    <xsl:template match="h:li[ following-sibling::h:li ]">
        <xsl:apply-templates/>
        <xsl:text>
 	    ,</xsl:text>
    </xsl:template>
    <xsl:template match="h:li">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="h:span">
        <xsl:apply-templates/>
    </xsl:template>


    <!-- QUALITY CHECKER	-->
    <xsl:template match="h:property">
	<xsl:text>

#	property not handled: </xsl:text>
        <xsl:value-of select="@key"/>
    </xsl:template>

    <!--				-->
    <!-- handling plain issueLink	-->
    <!-- as sent by Webhook		-->
    <!--				-->
    <xsl:template match="h:issueLink">

	<xsl:text>
	
</xsl:text>

        <xsl:text>&lt;</xsl:text>
        <xsl:value-of
                select=" kts:normalize_jira_url( h:property[@key='sourceIssue']/h:value/h:a/@self ) "/> <!-- inward issue -->
        <xsl:text>></xsl:text>

        <xsl:text> &lt;</xsl:text>
        <xsl:value-of select=" kts:normalize_jira_url( h:property[@key='type']/h:value/h:a/@href ) "/>
        <xsl:text>> </xsl:text>

        <xsl:text>&lt;</xsl:text>
        <xsl:value-of
                select=" kts:normalize_jira_url( h:property[@key='destinationIssue']/h:value/h:a/@self )"/> <!-- outward issue -->
        <xsl:text>></xsl:text>

        <xsl:text> .</xsl:text>

    </xsl:template>


    <!--              		-->
    <!-- handling Issue Link	-->
    <!-- as part of issue resource	-->
    <!--              		-->
    <xsl:template match="h:ilinks">
        <xsl:apply-templates select="h:ilink"/>
    </xsl:template>
    <xsl:template match="h:ilink">

	<xsl:text>
	
</xsl:text>

        <xsl:choose>
            <xsl:when test="@direction='outward'">
                <xsl:text>&lt;</xsl:text>
                <xsl:value-of select=" kts:normalize_jira_url( /h:html/h:body/h:top/h:self ) "/> <!-- this -->
                <xsl:text>></xsl:text>
            </xsl:when>
            <xsl:when test="@direction='inward'">
                <xsl:text>&lt;</xsl:text>
                <xsl:value-of
                        select=" kts:normalize_jira_url( h:property[@key='inwardIssue']/h:value/h:a/@self ) "/> <!-- inward issue -->
                <xsl:text>></xsl:text>
            </xsl:when>
        </xsl:choose>

        <xsl:text> &lt;</xsl:text>
        <xsl:value-of select=" kts:normalize_jira_url( h:property[@key='type']/h:value/h:a/@href ) "/>
        <xsl:text>&gt; </xsl:text>

        <xsl:choose>
            <xsl:when test="@direction='outward'">
                <xsl:text>&lt;</xsl:text>
                <xsl:value-of
                        select=" kts:normalize_jira_url( h:property[@key='outwardIssue']/h:value/h:a/@self ) "/> <!-- outward issue -->
                <xsl:text>></xsl:text>
            </xsl:when>
            <xsl:when test="@direction='inward'">
                <xsl:text>&lt;</xsl:text>
                <xsl:value-of select=" kts:normalize_jira_url( /h:html/h:body/h:top/h:self ) "/> <!-- this -->
                <xsl:text>></xsl:text>
            </xsl:when>
        </xsl:choose>

        <xsl:text> .</xsl:text>

    </xsl:template>

    <!--				-->
    <!-- Master Data Section	-->
    <!--				-->

    <!--                 -->
    <!-- Issue Link Type -->
    <!--                 -->
    <xsl:template mode="masterdata" match="h:ilink">
        <xsl:apply-templates mode="masterdata" select="//h:type"/>
    </xsl:template>
    <xsl:template mode="masterdata" match="h:type[ ancestor::h:ilink ]">

	<xsl:text>
&lt;</xsl:text>
        <xsl:value-of select=" kts:normalize_jira_url( ../../h:value/h:a/@href ) "/>
        <xsl:text>></xsl:text>

        <xsl:text> a owl:ObjectProperty </xsl:text>

        <xsl:text>
	; </xsl:text>

        <xsl:text> rdfs:label </xsl:text>

        <xsl:text>'''</xsl:text>
        <xsl:value-of select="h:property[@key='outward']/h:value"/>
        <xsl:text>'''</xsl:text>

        <xsl:text>
	. </xsl:text>

        <xsl:text>
&lt;</xsl:text>
        <xsl:value-of
                select=" concat(  kts:normalize_jira_url( ../../h:value/h:a/@href ), '/inverse'  ) "/> <!-- TODO: verify name in Conviz !! -->
        <xsl:text>></xsl:text>

        <xsl:text> a owl:ObjectProperty </xsl:text>

        <xsl:text>
	; </xsl:text>

        <xsl:text> rdfs:label </xsl:text>

        <xsl:text>'''</xsl:text>
        <xsl:value-of select="h:property[@key='inward']/h:value"/>
        <xsl:text>'''</xsl:text>

        <xsl:text>
	; </xsl:text>

        <xsl:text> owl:inverseOf </xsl:text>

        <xsl:text>&lt;</xsl:text>
        <xsl:value-of select=" kts:normalize_jira_url( h:value/h:a/@href ) "/>
        <xsl:text>></xsl:text>

        <xsl:text>
	. </xsl:text>

    </xsl:template>


    <xsl:template mode="masterdata" match="h:property[@key='status']">

	<xsl:text>
	
&lt;</xsl:text>
        <xsl:value-of select=" kts:normalize_jira_url( h:value/h:a/@href ) "/>
        <xsl:text>></xsl:text>
        <xsl:text> a  jira:systemtype:status </xsl:text>

        <xsl:text>
	; </xsl:text>
        <xsl:text> rdfs:label </xsl:text>
        <xsl:text>'''</xsl:text>
        <xsl:value-of select="h:value/h:a"/>
        <xsl:text>'''</xsl:text>

        <xsl:text>
	; </xsl:text>
        <xsl:text> jira:statusCategory </xsl:text>
        <xsl:text>&lt;</xsl:text>
        <xsl:value-of
                select=" kts:normalize_jira_url( h:value/h:status/h:property[@key='statusCategory']/h:value/h:a/@href ) "/>
        <xsl:text>></xsl:text>

        <xsl:text> .</xsl:text>

    </xsl:template>


    <xsl:template mode="masterdata" match="h:property[@key='issuetype']">

	<xsl:text>
	
&lt;</xsl:text>
        <xsl:value-of select=" kts:normalize_jira_url( h:value/h:a/@href ) "/>
        <xsl:text>></xsl:text>
        <xsl:text> a  jira:issuetype </xsl:text>

        <xsl:text>
	; </xsl:text>
        <xsl:text> rdfs:label </xsl:text>
        <xsl:text>'''</xsl:text>
        <xsl:value-of select="h:value/h:a"/>
        <xsl:text>'''</xsl:text>

        <xsl:call-template name="add_predicate_if_any">
            <xsl:with-param name="predicate" select=" 'rdfs:comment' "/>
            <xsl:with-param name="object" select=" h:value/h:a/@title "/>
            <xsl:with-param name="object_delimiter">'''</xsl:with-param>
        </xsl:call-template>

        <xsl:text> .</xsl:text>

    </xsl:template>


    <xsl:template match=" */h:property[ @key='projectTypeKey'] ">
        <xsl:text> # hier</xsl:text>
    </xsl:template>

    <!--
    <xsl:template mode="masterdata" match=" h:property[ @key='project' | ./h:value/*/h:property[ @key='projectTypeKey' ] ] " >
    -->
    <xsl:template mode="pmasterdata" match=" h:property "/>
    <xsl:template mode="pmasterdata"
                  match=" h:property[ @key='project'  or  h:value/*/h:property[ @key='projectTypeKey' ] ]">

	<xsl:text>
	
&lt;</xsl:text>
        <xsl:value-of select=" kts:normalize_jira_url( h:value/h:a/@href ) "/>
        <xsl:text>></xsl:text>
        <xsl:text> a  jira:project </xsl:text>

        <xsl:text>
	; </xsl:text>
        <xsl:text> jira:key </xsl:text>
        <xsl:text>'</xsl:text>
        <xsl:value-of select="h:value/*/h:property[@key='key']/h:value"/>
        <xsl:text>'</xsl:text>

        <xsl:text>
	; </xsl:text>
        <xsl:text> jira:id </xsl:text>
        <xsl:value-of select="h:value/*/h:property[@key='id']/h:value"/>

        <xsl:text>
	; </xsl:text>
        <xsl:text> rdfs:label </xsl:text>
        <xsl:text>'''</xsl:text>
        <xsl:value-of select="h:value/h:a"/>
        <xsl:text>'''</xsl:text>

        <xsl:text> .</xsl:text>

    </xsl:template>

    <!--			-->
    <!-- Helpers Section	-->
    <!--			-->

    <!--						-->
    <!-- wrapping a href (IRI) in angle brackets	-->
    <!--						-->
    <xsl:template match="h:a[@href]">
        <xsl:variable name="url">
            <xsl:choose>
                <xsl:when test="@self">
                    <xsl:value-of select=" @self "/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select=" @href "/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:text> &lt;</xsl:text>
        <xsl:value-of select=" kts:normalize_jira_url( $url ) "/>
        <xsl:text>&gt; </xsl:text>
    </xsl:template>

    <xsl:template name="add_predicate_object">
        <xsl:param name="predicate">jira:<xsl:value-of select=" @key "/>
        </xsl:param>
        <xsl:param name="object">
            <xsl:value-of select=" h:value "/>
        </xsl:param>
        <xsl:param name="object_delimiter"/>

        <xsl:if test=" $object != '' ">    <!--	don't import explicit field with empty value	-->
            <xsl:text>
	;  </xsl:text>
            <xsl:value-of select=" $predicate "/>
            <xsl:text>	</xsl:text>
            <xsl:value-of select=" $object_delimiter"/>
            <xsl:choose>
                <xsl:when test=" $object_delimiter = $triplesinglequotes ">
                    <xsl:choose>
                        <xsl:when test=" $singlesinglequote = substring(  $object, string-length( $object )  ) ">
                            <xsl:value-of
                                    select=" concat(  substring( $object, 1, string-length( $object ) - 1 ), '\', $singlesinglequote  )"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select=" $object "/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select=" $object "/>
                </xsl:otherwise>
            </xsl:choose>
            <xsl:choose>
                <xsl:when test=" $object_delimiter = '&lt;' ">
                    <xsl:text>&gt;</xsl:text>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select=" $object_delimiter"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:if>
    </xsl:template>

    <xsl:template name="add_predicate_if_any">
        <xsl:param name="predicate"/>
        <xsl:param name="object"/>
        <xsl:param name="object_delimiter"/>

        <xsl:if test=" $object != '' ">
		<xsl:text>
	; </xsl:text>
            <xsl:value-of select=" $predicate "/>
            <xsl:text> </xsl:text>
            <xsl:value-of select=" $object_delimiter"/>
            <xsl:value-of select=" $object"/>
            <xsl:choose>
                <xsl:when test=" $object_delimiter = '&lt;' ">
                    <xsl:text>&gt;</xsl:text>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select=" $object_delimiter"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:if>
    </xsl:template>

    <!--							-->
    <!-- https://symwork.atlassian.net/browse/SYMSKG-85	-->
    <!--							-->
    <!-- no function replace() in XSL 1.0 !			-->
    <!--							-->
    <func:function name="kts:normalize_jira_url">

        <xsl:param name="url"/>

        <xsl:variable name="pt1" select=" substring-before( $url, '/rest/api/' ) "/>
        <xsl:variable name="pt2" select=" substring-after ( $url, '/rest/api/' ) "/>
        <xsl:variable name="pt3" select=" substring-after ( $pt2, '/' ) "/>
        <func:result>
            <xsl:value-of select=" $pt1 "/>
            <xsl:text>/rest/api/latest/</xsl:text>
            <xsl:value-of select=" $pt3 "/>
        </func:result>

    </func:function>

</xsl:stylesheet>
