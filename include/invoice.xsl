<?xml version="1.0"?>

<xsl:stylesheet version="1.0"
	xmlns:xsl	="http://www.w3.org/1999/XSL/Transform"
	xmlns:h		="http://www.w3.org/1999/xhtml"
	xmlns:kts	="http://goegetap.name/ns-kts#"
>
<xsl:output method="xml" />

<!--                                      -->
<!--  -->
<!--                                      -->
<xsl:template match="/" >

<xsl:element name="html"	>
<xsl:element name="head"	>
<xsl:element name="title"	><xsl:apply-templates select=" h:html/h:body/h:fields/h:property[ @key = 'status' ] " /></xsl:element>
<link rel="stylesheet" type="text/css" href="/rechnung-x.css" />
</xsl:element> <!-- head -->
<xsl:element name="body"	>

<header>
 	<div id="sender-right">
		 <p>
		 Boran Gögetap
		 <br/>
		 Am Sagbach 25a
		 <br/>
		 83661 Lenggries
		 </p>
		 <p>0800 299 79 24</p>
		 <p id="email">boran@goegetap.name</p>
		 <p id="web">www.goegetap.name</p>
 	</div>
	<div id="address-window">
		<p id="sender-top">Boran Gögetap ● Am Sagbach 25a ● D-83661 Lenggries</p>
		<div id="recipient">
			<xsl:apply-templates select=" h:html/h:body/h:fields/h:property[ @key = 'customfield_11138' ] " />
		</div>
 	</div>
</header>

<section id="spec">
<div id="record-type"><xsl:apply-templates select=" h:html/h:body/h:fields/h:property[ @key = 'status' ] " /></div>
<div id="record-key-label">Nr.</div>
<div id="record-key"><xsl:value-of select="h:html/h:body/h:top/h:key" /></div>
<div id="record-date-label"><xsl:apply-templates select=" h:html/h:body/h:fields/h:property[ @key = 'status' ] " />sdatum</div>
<div class="date" id="record-date">
	<xsl:call-template name="format-date">
		<xsl:with-param name="iso-date-string" select=" h:html/h:body/h:fields/h:property[ @key = 'customfield_11137' ]/h:value " />
	</xsl:call-template>
</div>

<!-- TODO: implement Leistungszeitraum
<div id="service-begin-label"	>Leistungszeitraum von</div>	<div id="service-begin"	>x</div>
<div id="service-end-label"		>bis</div>						<div id="service-end"	>y</div>
 -->

<div id="summary"><xsl:value-of select=" h:html/h:body/h:fields/h:property[ @key = 'summary' ]/h:value " /></div>

<div id="description">
	<xsl:copy-of select=" /h:html/h:body/h:renderedFields/h:description " />
</div>

<div id="net-label"		>Nettosumme</div>
<xsl:apply-templates select=" //h:property[ @key = 'customfield_11133' ] " />

<div id="tax-label"		>Umsatzsteuer</div>
<div id="tax-percent"	><xsl:value-of select=" //h:customfield_11132/h:property[ @key='value']/h:value " /> %</div>

<xsl:call-template name="format-currency-value" >
	<xsl:with-param name="currency-input">
		<xsl:value-of select="  (//h:property[ @key = 'customfield_11127' ]/h:value)  -  (//h:property[ @key = 'customfield_11133' ]/h:value)  " />
	</xsl:with-param>
	<xsl:with-param name="id">tax-amount</xsl:with-param>
</xsl:call-template>

<div id="grs-label"		><xsl:apply-templates select=" h:html/h:body/h:fields/h:property[ @key = 'status' ] " />ssumme</div>
<xsl:apply-templates select=" //h:property[ @key = 'customfield_11127' ] " />

<!-- TODO: implement due date
<div id="due-date-label">Zahlungsziel</div>
<div id="due-date"		></div>
 -->

<div id="mfg">Mit freundlichen Grüßen,</div>
<div id="signature">(Boran Gögetap)</div>

</section>


<div id="accounting-details" >

<p>
 BIC : GENODEF1ETK &#160;&#160;&#160;&#160;EthikBank
 <br/>
 IBAN: DE 7283094495 000 3310191
 <br/>
 USt-IdNr (VAT Id): DE 130347186
</p>
</div>

</xsl:element> <!-- body -->
</xsl:element> <!-- html -->
</xsl:template>

<xsl:template match=" h:property[ @key = 'status'									] ">sonstiger Beleg</xsl:template>
<xsl:template match=" h:property[ @key = 'status' and h:value/h:a =	'identifiziert' ] ">Angebotsentwurf</xsl:template>
<xsl:template match=" h:property[ @key = 'status' and h:value/h:a =	'angeboten'		] ">Angebot</xsl:template>
<xsl:template match=" h:property[ @key = 'status' and h:value/h:a =	'berechnet'		] ">Rechnung</xsl:template>

<!--           -->
<!-- Anschrift -->
<!--           -->
<xsl:template match=" h:property[ @key = 'customfield_11138' ] " >
	<xsl:apply-templates select=" h:value " />
</xsl:template>

<!--              -->
<!-- Beschreibung -->
<!--              -->
<xsl:template match=" h:property[ @key = 'description' ] " >
	<xsl:apply-templates select=" h:value " />
</xsl:template>

<!--               -->
<!-- Betragsfelder -->
<!--               -->
<xsl:template match=" h:property[ @key = 'customfield_11133' or @key = 'customfield_11127' ] " name="format-currency-value" >
	<xsl:param name="currency-input"	select=" h:value " />
	<xsl:param name="id"				select=" @key " />
	<xsl:element name="div">
		<xsl:attribute name="class">currency</xsl:attribute>
		<xsl:attribute name="id" ><xsl:value-of select=" $id " /></xsl:attribute>
		<xsl:value-of select=" format-number( $currency-input, '#,###.00' ) " />
	</xsl:element>
</xsl:template>


<xsl:template match="text()">
	<xsl:param name="text" select="." />
	
	<xsl:if test=" normalize-space($text) ">

		<xsl:variable name="startText"	select="substring-before(concat($text,'&#10;'),'&#10;')" />
		<xsl:variable name="nextText"	select="substring-after($text,'&#10;')" />
		<p>
		<xsl:value-of select="$startText" />
		</p>
	
		<xsl:apply-templates select=".">
			<xsl:with-param name="text" select="$nextText" />
		</xsl:apply-templates>

	</xsl:if>
</xsl:template>


<xsl:template name="format-date">
	<xsl:param name="iso-date-string" />
	<xsl:value-of select=" substring( $iso-date-string, 9, 2 ) " />
	<xsl:text>.</xsl:text>
	<xsl:value-of select=" substring( $iso-date-string, 6, 2 ) " />
	<xsl:text>.</xsl:text>
	<xsl:value-of select=" substring( $iso-date-string, 1, 4 ) " />
</xsl:template>


</xsl:stylesheet>