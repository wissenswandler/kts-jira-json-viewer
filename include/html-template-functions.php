<?php
function iterate_assoc_array( $var, $array_key, array $blacklist = [] ) {

	if( gettype( $array_key ) != "integer" )
		echo "\n<" . $array_key . '>';

	foreach( $var[$array_key] as $key => $value) {
		if( $value != null and $value != "" ) {
			if( ! in_array( $key, $blacklist ) ) {
				
				renderIterable( $var[ $array_key ], $key );
			}
		}
	}
	if( gettype( $array_key ) != "integer" )
		echo "\n</" . $array_key . '>';
}

function renderIterable( $var, $key ) {
	
	if( ! array_key_exists( $key, $var ) ) return;

	echo "\n <property" . ' key="' . $key . '">';
	
	if( gettype( $key ) != "integer" )
		echo "\n  " . '<key name="' . $key . '">' . $key . '</key>';
	
	echo "\n  <value>";
	
	$value = $var[ $key ];

	switch( gettype( $value ) ) {
		case "array":

			if( array_key_exists( 'fields', $value) and array_key_exists( 'status', $value[ 'fields' ] ) ) {
				// this seems to be a reference to an issue
				hyperlink_issue( $value );
			}
			elseif( array_key_exists( 'self', $value ) ) 
			{
				// aha, we should consider this an entity / RDF resouce
				
				echo '<a href="' . $value['self'] . '"' ;
				if( array_key_exists( 'description', $value ) ) echo ' title="' . htmlspecialchars( $value['description'], ENT_XML1 | ENT_COMPAT, 'UTF-8') . '"' ;
				
				if( array_key_exists( 'name', $value ) )
				{
				echo '>' ;
				echo wrap_cdata( $value['name'] );
				echo '</a';
				}
				else echo '/';

				echo '>';
				
				iterate_assoc_array( $var, $key, ["self", "description", "name",   "avatarId", "avatarUrls", "iconUrl", "active", "timeZone"] );
			}
			elseif( has_string_keys( $value ) ) {
				// some sort of an object but not an entiity / resource (no self)
				iterate_assoc_array( $var, $key );
			}
			else {
				// no string keys, so it should just be a set
				
				switch( count( $value) ) {
					case -1:
					
						renderIterable( $value, 0 );
						break;
						
					default:
				
						echo '<ul>';
						foreach( $value as $key => $enum ) {
							echo '<li><span>';
							renderIterable( $value, $key );
							echo '</span></li>';
						}
						echo '</ul>';
				}
			}
			break;
		default:
			echo render_safe_xml_string( $value );
//			if( preg_match( '#^[A-Z]+-[0-9]+$#',  $value ) )
//				echo '<a href="' . $value . '">' . $value . '</a>';
//			elseif( preg_match( '#.*[<&].*#',  $value ) )
//			{
//				echo "<![CDATA[";
//				echo $value;
//				echo "]]>";
//			}
//			else
//				echo $value;
	}
	echo "</value>\n </property>";
}

/*
 * if the string value is meant to be used in quotes, then quotes need to be escaped
 */
function render_safe_xml_string( $value, $quoted = false )
{
  if( preg_match( '#.*[<&].*#',  $value ) )
  {
    return "<![CDATA[" . $value . "]]>";
  }
  elseif( $quoted )
    return str_replace( '"', '&quot;', $value ) ;
  else
    return                             $value   ;
}

function wrap_cdata( $value )
{
	return "<![CDATA[" . $value . "]]>";
}

function hyperlink_issue( $i ) {
	echo '<a href="'  . $i['key'] . '" self="' . $i['self'] . '">' ;
	echo wrap_cdata( $i['fields']['summary'] ) ;
	echo '</a>' ;
}

function renderLinkedIssue( $link ) 
{
	renderLinkedIssueSide( $link, 'outward' );
	renderLinkedIssueSide( $link,  'inward' );
}

function renderLinkedIssueSide( $link, string $direction )
{
    if(  array_key_exists( $direction . "Issue" , $link )  ) 
	{
		echo "\n" . '<ilink direction="' . $direction . '">';
		renderIterable( $link, "type" ); 
		renderIterable( $link, $direction . "Issue" ); 
		renderIterable( $link[ $direction . "Issue" ]['fields'], 'status' ); 
		echo '</ilink>' ;
	}	
}

function has_string_keys(array $array) {
  return count(array_filter(array_keys($array), 'is_string')) > 0;
}
?>
