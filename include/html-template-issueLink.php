<?php echo '<?xml version="1.0" ?>' ; ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="utf-8"/>
<?php	include( 'html-template-functions.php' );	?>
<title><?php echo $subject['sourceIssueId'] . " " . render_safe_xml_string( $subject['issueLinkType']['outwardName'] ) . " " . $subject['destinationIssueId'] ?></title>
<link rel="stylesheet" type="text/css" href="/s.css" />
</head>
<body>
<issueLink><?php
	renderIterable( $subject, 'id'					);	
?>
 <property key="sourceIssue">
  <key name="sourceIssue">sourceIssue</key>
  <value><a href="../browse/<?php echo $subject['sourceIssueId']?>" self="https://<?php echo $_GET["domain"] ?>/rest/api/2/issue/<?php echo $subject['sourceIssueId']?>"><?php echo $subject['sourceIssueId']?></a></value>
 </property>

 <property key="type">
  <key name="type">type</key>
  <value><a href="https://<?php echo $_GET["domain"] ?>/rest/api/2/issueLinkType/<?php echo $subject['issueLinkType']['id'] ?>"><?php echo $subject['issueLinkType']['outwardName'] ?></a>
   <type>
<?php 
	renderIterable( $subject['issueLinkType'], 'id'					);
	renderIterable( $subject['issueLinkType'], 'name'					);
	renderIterable( $subject['issueLinkType'], 'outwardName'			);
	renderIterable( $subject['issueLinkType'], 'inwardName'			);
?>	
</type></value>
 </property>	

 <property key="destinationIssue">
  <key name="destinationIssue">destinationIssue</key>
  <value><a href="../browse/<?php echo $subject['destinationIssueId']?>" self="https://<?php echo $_GET["domain"] ?>/rest/api/2/issue/<?php echo $subject['destinationIssueId']?>"><?php echo $subject['destinationIssueId']?></a></value>
 </property>   

</issueLink>
<?php
if( $_GET["accept"] != "turtle" && $_GET["transfer"] != "turtle" )
{
	echo "\n<hr/><p/>";
	echo '<a href="' . $_SERVER['REQUEST_URI'] . '?accept=turtle">Audit: RDF-Turtle Äquivalent</a>';
}
?>
</body>
</html>

<?php
/*

brauche: 

<ilink direction="inward">
 <property key="type">
  <key name="type">type</key>
  <value><a href="https://knowhere.atlassian.net/rest/api/2/issueLinkType/10407">iwie</a>
<type>
 <property key="id">
  <key name="id">id</key>
  <value>10407</value>
 </property>
 <property key="inward">
  <key name="inward">inward</key>
  <value>hängt irgendwie zusammen mit</value>
 </property>
 <property key="outward">
  <key name="outward">outward</key>
  <value>hängt irgendwie zusammen mit</value>
 </property>
</type></value>
 </property>
 <property key="inwardIssue">
  <key name="inwardIssue">inwardIssue</key>
  <value><a href="EX-17" self="https://knowhere.atlassian.net/rest/api/2/issue/15277">bums</a></value>
 </property>
 <property key="status">
  <key name="status">status</key>
  <value><a href="https://knowhere.atlassian.net/rest/api/2/status/1" title="Der Vorgang ist offen die zugewiesene Person kann mit der Arbeit daran beginnen.">Offen</a>
<status>
 <property key="id">
  <key name="id">id</key>
  <value>1</value>
 </property>
 <property key="statusCategory">
  <key name="statusCategory">statusCategory</key>
  <value><a href="https://knowhere.atlassian.net/rest/api/2/statuscategory/2">Aufgaben</a>
<statusCategory>
 <property key="id">
  <key name="id">id</key>
  <value>2</value>
 </property>
 <property key="key">
  <key name="key">key</key>
  <value>new</value>
 </property>
 <property key="colorName">
  <key name="colorName">colorName</key>
  <value>blue-gray</value>
 </property>
</statusCategory></value>
 </property>
</status></value>
 </property></ilink>
	
	
	habe:
	
 <property key="id">
  <key name="id">id</key>
  <value>14757</value>
 </property>
 <property key="sourceIssueId">
  <key name="sourceIssueId">sourceIssueId</key>
  <value>15227</value>
 </property>
 <property key="destinationIssueId">
  <key name="destinationIssueId">destinationIssueId</key>
  <value>15226</value>
 </property>

 <property key="issueLinkType">
  <key name="issueLinkType">issueLinkType</key>
  <value>

*/
?>
