<?php echo '<?xml version="1.0" ?>' ; ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="utf-8"/>
<title><?php echo $subject['key'] . " : " . htmlspecialchars( $subject['fields']['summary'], ENT_XHTML , 'UTF-8'  ) . " - KTS Viewer" ?></title>
<link rel="stylesheet" type="text/css" href="/s.css" />
<link rel="icon" href="/favicon.ico" />
</head>
<body>
<?php
	include( 'html-template-functions.php' );	
	echo   "<top>";
	echo "\n <id>" .   $subject['id'  ]   . '</id>';
	echo "\n <key>" .  $subject['key' ]  . '</key>';
	echo "\n <self>" . $subject['self'] . '</self>';
	echo "\n</top>";
	echo "\n<pields>";
	renderIterable( $subject['fields'], "summary"		);
	echo "\n</pields>";
	
	iterate_assoc_array
	(
		$subject, 'fields', 
		[
			"summary", "description", "issuelinks"		// rendered explicitly in code above and below
			,
			"votes"						// seems to be an empty array ALWAYS
			,
			"workratio", "progress", "aggregateprogress"	// not interested in
			,
			"customfield_10000", "customfield_10009"
			,
			"customfield_10700", "customfield_10800"	// some builtins (Jira Software or Portfolio) with little value
			,
			"customfield_10014", "customfield_10018"	// former Epic-Link, now replaced by much cleaner "parent" field
			,
			"customfield_10022"				// weird encoded values
		]
	);

	echo "\n<pields>";
	renderIterable( $subject['fields'], "description" );
	echo "\n</pields>\n\n";
?>
<ilinks><?php foreach( $subject['fields']['issuelinks'] as $link ) renderLinkedIssue( $link ); ?>
</ilinks>
<?php

if
(
 ( ! array_key_exists( "accept"  , $_GET ) || $_GET[ "accept"   ] != "turtle" )
 &&
 ( ! array_key_exists( "transfer", $_GET ) || $_GET[ "transfer" ] != "turtle" )
)
{
	echo "\n<hr/><p/>";
	echo '<a target="turtle" href="' . $_SERVER['REQUEST_URI'] . '?accept=turtle">Audit: RDF-Turtle Äquivalent</a>';
	echo "\n<p/>";
	echo '<a target="rdf"    href="' . $_SERVER['REQUEST_URI'] . '?transfer=turtle">Import -> RDF-DB !</a>';

    if( $subject['fields']['issuetype']['self'] == 'https://knowhere.atlassian.net/rest/api/2/issuetype/10300' ) // Ausgangsrechnung
    {
    	echo "\n<hr/>";
    	echo '<a href="' . str_replace( 'browse', 'rechnung', $_SERVER['REQUEST_URI'] ) . '">Spezialseite für Ausgangsrechnung</a>';
    }

    //	echo "\n<hr/>";
    //	echo '<a href="http://sparql.wissenswandler.de/w/repositories/webhook/explore?resource=&lt;' . $subject['self'] . '>" target="sparql">go SPARQL...</a>';
	
}
?>
</body>
<masterdata/>
</html>
