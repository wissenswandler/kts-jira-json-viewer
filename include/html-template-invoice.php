<?php 
	echo '<?xml version="1.0" encoding="UTF-8"?>';
	include( 'html-template-functions.php' );	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $subject[key] . " : " . $subject[fields][summary] ?></title>
<link rel="stylesheet" type="text/css" href="/rechnung.css" />
<link rel="icon" href="/<?php echo $_GET["domain"] ?>/favicon.png" />
</head>
<body>
<?php

	echo   "<top>";
	echo "\n <id>"   . $subject[id]   . '</id>';
	echo "\n <key>"  . $subject[key]  . '</key>';
	echo "\n <self>" . $subject[self] . '</self>';
	echo "\n</top>";

	iterate_assoc_array
	(
		$subject, 'fields', 
		[
			"issuelinks", 		// rendered explicitly in code above and below
			"votes",										// seems to be an empty array ALWAYS
			"watches",	// has total count but no people
			"workratio", "progress", "aggregateprogress",	// not interested in
			"customfield_10000", "customfield_10009", "customfield_10700", "customfield_10800",	// some builtins (Jira Software or Portfolio) with little value
			
			lastViewed, updated, issuetype, project, lastviewed, priority, assignee, customfield_10005, // epic
			creator, subtasks, customfield_11130, // SKR04-Konto	
		]
	);

	$desc = str_replace( chr(10), '', str_replace( chr(13), '\n', $subject[fields][description] ) );
	$render_request = '{"rendererType":"atlassian-wiki-renderer","unrenderedMarkup":"' . $desc . '"}';
	
	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL,			"https://knowhere.atlassian.net/rest/api/1.0/render" );
	curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST,	'POST');
	curl_setopt($ch, CURLOPT_HTTPHEADER, 	array( 'Content-Length: '.strlen( $render_request ), 'Content-Type: application/json' ) );
	curl_setopt($ch, CURLOPT_POSTFIELDS,    $render_request );
	
	$result=curl_exec ($ch);
	
	echo   "<renderedFields>";
	echo "\n <description>"   . $result . '</description>';
	echo "\n</renderedFields>";

	
	echo "\n<hr/>";
	echo '<a href="' . str_replace( 'rechnung', 'browse', $_SERVER[REQUEST_URI] ) . '">allgemeine Seite für Vorgänge</a>';
?>
</body>
</html>