<?php

include 'domain.php';

function warning_handler($errno, $errstr) { 
	throw new Exception( $_SERVER['REQUEST_URI'] . " -- " . $errstr );
}
/*
 * understand the requested resource
 */
if( $_GET["issue"] )
{
	$requested_resource_type	= 'issue';
}
elseif( $_GET["issueLink"] )
{
	$requested_resource_type	= 'issueLink';
}
else
{
	$requested_resource_type	= 'others';
}
$requested_resource_id		= $_GET[ $requested_resource_type ];

$self = $_GET["domain"] . '/rest/api/2/' . $requested_resource_type . '/' . $requested_resource_id;

try {
	set_error_handler("warning_handler", E_WARNING);
	$filecontent = file_get_contents( $basepath . $self );
	restore_error_handler();
}
catch( Exception $e)
{
	error_log( $e );
	http_response_code(404);
	include( 'include/error.php' );	
	die();	
}

try {
	set_error_handler("warning_handler", E_WARNING);
	$subject = json_decode( $filecontent, true);
	restore_error_handler();
}
catch( Exception $e)
{
	error_log( $e );
	http_response_code(500);
	include( 'include/error.php' );	
	die();	
}

/*
 * render requested resource to XHTML
 */
ob_start();
include 'include/html-template-' . $requested_resource_type . '.php';
$xhtmlstring = ob_get_contents();
ob_end_clean();

if(  array_key_exists( "accept"  ,    $_GET )    && $_GET[    "accept"      ] == "turtle"
  or array_key_exists( "transfer",    $_GET )    && $_GET[    "transfer"    ] == "turtle"  
  or array_key_exists( "HTTP_ACCEPT", $_SERVER ) && $_SERVER[ "HTTP_ACCEPT" ] == 'text/turtle' 
)
{
	/*
	 * translate resource from XHTML to RDF
	 */
	$xslDoc = new DOMDocument();
	$xslDoc->load( "include/rdf.xsl");

	$xmlDoc = new DOMDocument();
	
try {
	set_error_handler("warning_handler", E_WARNING);
	$xmlDoc->loadXML( $xhtmlstring );
	restore_error_handler();
}
catch( Exception $e)
{
	error_log( $e );
	http_response_code(500);
	//echo "ERROR in following XML: \n" ;
	//echo $xhtmlstring ;
	die() ;
}

	$xmlDoc->loadXML( $xhtmlstring );

	$proc = new XSLTProcessor();
	$proc->importStylesheet($xslDoc);

	if(  array_key_exists('reduce', $_GET )  ) $proc->setParameter( "", "reduce", $_GET['reduce'] );
				
	$rdf = $proc->transformToXML($xmlDoc);
				
	if( array_key_exists( "transfer", $_GET ) && $_GET["transfer"] == "turtle" )
	{
		/*
		 * post RDF into triple store
		 */
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL,            $sparql_update_endpoint );
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt($ch, CURLOPT_POST,           1 );
		curl_setopt($ch, CURLOPT_POSTFIELDS,     $rdf ); 
		curl_setopt($ch, CURLOPT_HTTPHEADER,     array('Content-Type: text/turtle') ); 

		$result=curl_exec ($ch);
		curl_close ($ch);

		if (curl_errno($ch)) 
		{
		    echo 'curl error:' . curl_error($ch);
		}
		else
		{
		  $curl_http_status = curl_getinfo( $ch, CURLINFO_RESPONSE_CODE ) . ' ';
		  switch( $curl_http_status )
		  {
		    case 204:
		      // perfectly fine, 
		      // no output so that batch import can focus on exceptions
		      break;
		    default:
		      echo "\nRDF HTTP status (error): " . $curl_http_status ;
		      echo "\nfor " . $_SERVER['REQUEST_URI'] ;
		      echo "\nRDF response:" ;
		      echo "\n" . $result . "\n" ;
		  }
		}	// end curl_error
	}	// end transfer
	else
	{
		/*
		 * return RDF (for test purposes)
		 */
		echo $rdf;
	}
}
else
{
	/*
	 * return XHTML for presentation in browser
	 */
	header('Content-type: application/xhtml+xml');
	echo $xhtmlstring;
}
?>
